#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)  # fixed, i = 9, cycle length = 20

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)  # i = 171, cycle length = 125

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)  # 206, cycle length = 89

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)  # 937, cycle length = 174

    # ************ my unit tests ************
    def test_eval_5(self):
        v = collatz_eval(343, 993)
        self.assertEqual(v, 179)

    def test_eval_6(self):
        v = collatz_eval(434, 474)
        self.assertEqual(v, 129)

    def test_eval_7(self):
        v = collatz_eval(178, 247)
        self.assertEqual(v, 128)

    def test_eval_8(self):
        v = collatz_eval(74, 755)
        self.assertEqual(v, 171)

    def test_eval_9(self):
        v = collatz_eval(68, 451)
        self.assertEqual(v, 144)

    def test_eval_10(self):
        v = collatz_eval(128, 668)
        self.assertEqual(v, 145)

    def test_eval_11(self):
        v = collatz_eval(535, 855)
        self.assertEqual(v, 171)

    def test_eval_12(self):
        v = collatz_eval(483, 912)
        self.assertEqual(v, 179)

    def test_eval_13(self):
        v = collatz_eval(131, 545)
        self.assertEqual(v, 144)

    def test_eval_14(self):
        v = collatz_eval(906, 509)
        self.assertEqual(v, 179)

    def test_eval_15(self):
        v = collatz_eval(1, 999)
        self.assertEqual(v, 179)

    def test_eval_16(self):
        v = collatz_eval(1000, 1999)
        self.assertEqual(v, 182)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )  # fixed


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()

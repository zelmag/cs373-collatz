after a successful build on GitLab CI, commit and push to your private code repo, resolving an issue in the issue tracker
make at least 5 commits to the private code repo, one for each bug or feature as you develop the solution
if you cannot describe your changes in a sentence, you are not committing often enough
make meaningful commit messages identifying the corresponding issue in the issue tracker of the private code repo (see automatic issue closing)
set up GitLab issues
create lists: (e.g. Backend, Customer, Frontend, Grader, etc.)
add at least 10 issues from this workflow
add at least 5 more issues, one for each bug or feature, with a good description and a label as you develop the solution
run the given unit tests and acceptance tests, confirm the expected successes, and add, commit, and push to the private code repo
before continuing, confirm a successful build on GitLab CI, and then add, commit, and push to your private code repo.
fix the given unit tests and acceptance tests, confirm the expected failures
write unit tests and acceptance tests that test corner cases and failure cases, and confirm the expected failures and add, commit, and push to the private code repo
implement and debug the simplest possible solution with assertions that check pre-conditions, post-conditions, argument validity, and return-value validity, until all tests pass, and add, commit, and push to the private code repo
before continuing, confirm a successful build on GitLab CI, and then add, commit, and push to your private code repo.
implement and debug any optimizations until all tests pass and add, commit, and push to the private code repo.
before continuing, confirm a successful build on GitLab CI, and then add, commit, and push to your private code repo.
format your code with black
create a git log of your commits to the private code repo
fork and clone the public test repo onto your local directory
run checktestdata
copy your acceptance tests to your clone of the public test repo, rename the files, do a git pull to synchronize your clone, and then add, commit, and make a merge request to the public test repo
the filenames MUST start with GitLabID(owner of the repo)- in the public test repo
run pydoc to document the interfaces to your functions/methods (Collatz.html)
create inline comments if you need to explain the why of a particular implementation
use a consistent coding convention with good variable names (see Google Python Style Guide)
create a README file with the Git SHA of the master branch of the private code repo, which can be obtained as follows:
git rev-parse HEAD
submit the assignment on Canvas